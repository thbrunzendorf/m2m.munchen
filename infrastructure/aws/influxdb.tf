variable "influxdb_url" {
  description = "Hostname or IP address of influxdb"
  default = "http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com:8086"
}

variable "influxdb_database" {
  description = "Database name for influxdb"
  default = "munchen"
}
