#cloud-config
package_upgrade: true

packages:
  - awscli
  - nodejs

runcmd:
  - mkdir -p /home/ubuntu/app
  - AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${router_service_bucket}/${router_tarball}" /home/ubuntu/app/
  - cd /home/ubuntu/app
  - su -l -c "tar xzf /home/ubuntu/app/${router_tarball}" ubuntu
  - LOBSTERS_DNS_NAME=${lobsters_dns_name} MODLOG_DNS_NAME=${modlog_dns_name} node "/home/ubuntu/m2m.router/index.js"
